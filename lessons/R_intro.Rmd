---
title: "Introduction to R"
subtitle: 30 Mins.
output:
  pdf_document: default
  html_document: default
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Questions
- What are the benefits of using a programming language instead of another tool?
- How do I get started writing code?
- How can I use R to read and interact with data?

## Learning Outcomes
- Gain familiarity with RStudio IDE
- Understand some basic R syntax and capabilities
- Create, manipulate, and remove variables
- Create functions
- Download and load packages
- Read and write data to and from a file

## Timing
- Teaching: 30 min
- Exercises: 0 min

## Teaching Notes
### Intro to RStudio IDE
- look at the panels and what they can do

### Using R's built-in capabilities
Using R as a calculator
```{r}
4 + 4

10 * 10
```

It even respects order of operations

```{r}
4 * (8 + 2)

4 * 8 + 2
```

We can also do comparisons
```{r}
4 < 9

8 > 10

10 == 10

10 >= 10

5 != 5
```

There are some mathematical functions built right in, including basic trigonometric functions

```{r}
cos(4)
```

Notice the notation we use, `function_name(argument)`. It's critical to understanding R, and we'll use that later.

R will throw an error if we give it any unacceptable inputs, too.

```{r error = TRUE}
10 + "hello world"
```

### Variables
```{r}
new_int <- 4
new_int
```

'new_int' is a name we made up - variables can be called whatever is easy for you to remember.


```{r}
cos(new_int) # Finds the cosine of the value stored in new_int
cos(4)
```

### Functions
```{r}
new_fun <- function(x) { # Creates a function called new_fun that takes one argument, "x"
  my_int <- x # Stores "x" as my_int
  your_int <- my_int * 2 # Stores x * 2 as your_int
  cat("My integer is ",my_int," and your integer is ",your_int) # Prints out a result telling us each our numbers
}

new_fun(4) # Try it out with any number
```

### Cleaning up environment
It's good to keep a clean working environment, so let's get rid of some of the clutter we've generated
```{r}
rm(new_int)
rm(new_fun)

# Get rid of everything at once
rm(list = ls())
```

### Packages
When people have put together their own functions and datasets that they feel will be useful to others, they bundle them up into packages. We can install packages with the RStudio GUI, or using a simple command:
```{r eval=FALSE}
install.packages("tidyverse")
```

From there, we need to load the package into our working environment to be able to access the data and functions that are built in. 
```{r message=FALSE}
library(tidyverse)
```

Let's look at some of the data that we now have access to.
```{r}
data(starwars)
starwars
```

### Working with Data
That Star Wars data set is what we call a "data frame." Data frames are among the most common data structures you'll deal with in R, so it's good to know a little about them. It looks a bit like an Excel sheet, or any other spreadsheet you may have seen before, but it has some special properties. A data frame is a *list* of *vectors*, which must all be of equal length. 

#### Vectors
You don't need to know about a list right now except that it's a very flexible data structure that can hold all kinds of things. Vectors are *groups of like objects*, which we can do some powerful stuff with in R. Here's just a quick example:

```{r}
vec <- c(2, 6, 4, 9, 2.5, 18)

vec * 10
vec^2
sqrt(vec)
```

This works on the vectors that make up data frames, too. For instance:
```{r}
starwars$height
starwars$height * 2
```

We can even create new variables using this behavior
```{r}
starwars$doubleheight <- starwars$height * 2

glimpse(starwars)
```


The trick is that vectors must all be the same type of data. 
```{r error = TRUE}
typeof(starwars$height)

test_vec <- c(starwars$height, "hello")

typeof(test_vec)

test_vec * 2
```

#### Loops
There's a common feature of other programming languages that sometimes takes a back seat in R, but can still be useful: the For loop.
```{r eval=FALSE}
for(i in starwars$height) {
  print(i * 2)
}

```

As a general rule in R, vectorized operations are quicker and easier on your computer. That doesn't mean For loops are always wrong, though.


#### Reading Data
This is all well and good, but let's read some data in. We'll practice with a fake dataset before moving onto the bigger one we've been using.

```{r message=FALSE}
df <- read_csv("../data/bd_data.csv")
```

```{r}
df
```


#### Examining Data
```{r}
ncol(df)

names(df)

nrow(df)

glimpse(df)
```

#### Writing Data
Let's get a subset of that just to practice writing it out now.

```{r}
df_sub <- head(df)

df_sub
glimpse(df_sub)

write_csv(df_sub, "../data/subset.csv")
```


## Activities
N/A

## Handouts
N/A

## Readings
### Optional
- Wickham, H., & Grolemund, G. (2017). R for Data Science: Import, Tidy, Transform, Visualize, and Model Data. Sebastopol, CA: O’Reilly Media.
Retrieved from <http://r4ds.had.co.nz/>

